var autoprefixerOptions = { overrideBrowserslist: ['last 3 versions', '> 5%', 'Firefox ESR'] };
var autoprefixer = require('autoprefixer');

module.exports = function(gulp, browserSync, plugins, paths) {
  return function () {
    var stream =
        gulp.src(paths.src.baseDir + '/less/[!_]*.less')
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.less())
        .pipe(plugins.postcss([ autoprefixer(autoprefixerOptions) ]))
        .pipe(plugins.sourcemaps.write('../css'))
        .pipe(gulp.dest(paths.build.styles))
        .pipe(browserSync.stream())
        .on('error', plugins.util.log);
        return stream;
      };
};
